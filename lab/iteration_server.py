# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config
import socket


def server(logger):
    """ Serwer iteracyjny zwracajacy kolejny numer połączenia
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('194.29.175.240', 9992)  # TODO: zmienić port!

    # Ustawienie licznika na zero
    count = 0

    # Tworzenie gniazda TCP/IP
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Powązanie gniazda z adresem
    s.bind(server_address)

    # Nasłuchiwanie przychodzących połączeń
    logger.info(u'tworzę serwer na {0}:{1}'.format(*server_address))
    s.listen(1)
    try:
        while True:
            # Czekanie na połączenie
            logger.info(u'czekam na połączenie')


            # Nawiązanie połączenia
            addr = ('', '')

            conn, addr = s.accept()

            logger.info(u'połączono z {0}:{1}'.format(*addr))

            # Podbicie licznika
            count += 1
            try:
                # Wysłanie wartości licznika do klienta
                conn.send('{0}'.format(count))
                logger.info(u'wysłano {0}'.format(count))

            finally:
                # Zamknięcie połączenia
                conn.close()
                logger.info(u'zamknięto połączenie')

    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('iteration_server')
    server(logger)
    sys.exit(0)