def tryParseInt(str):
    isValid = False
    try:
        returnValue = int(str)
        isValid = True
    except ValueError:
        returnValue = 0
        print "Wprowadz wlasciwa liczbe"
    return returnValue, isValid

keepProgramRunning = True

while keepProgramRunning:
    print "\nWitaj w serwerowym kalkulatorze. Wybierz co chcesz zrobic:"

    print "1: Dodawanie"
    print "2: Wyjdz z aplikacji"

    choice = raw_input()

    if choice == "1":

        isFirstNumberValid = False
        while not isFirstNumberValid:
            str = raw_input("Wprowadz pierwsza liczbe: ")
            firstNumber, isFirstNumberValid = tryParseInt(str)

        isSecondNumberValid = False
        while not isSecondNumberValid:
            str = raw_input("Wprowadz druga liczbe: ")
            secondNumber, isSecondNumberValid = tryParseInt(str)


        print "\nWynik dodawania:"
        print firstNumber + secondNumber

    elif choice == "2":
        print "Koniec aplikacji"
        keepProgramRunning = False

