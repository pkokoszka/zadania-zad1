# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config

def tryParseInt(str):
    isValid = False
    try:
        returnValue = int(str)
        isValid = True
    except ValueError:
        returnValue = 0
        print "Wprowadz wlasciwa liczbe"
    return returnValue, isValid

def client(message, message2):

    server_address = ('194.29.175.240', 4541)
    # Tworzenie gniazda TCP/IP
    # TODO: wstawić kod tworzący nowe gniazdo sieciowe
    s = socket.socket()

    # Połączenie z gniazdem nasłuchującego serwera


    #logger.info(u'nawiązuję połączenie z {0} na porcie {1}'.format(*server_address))
    # TODO: połączyć się z gniazdem serwera
    s.connect(server_address)

    try:
        # Wysłanie danych
        #logger.info(u'wysyłam "{0}"'.format(message))
        # TODO: wysłać wiadomość message do serwera
        s.sendall(message)
        # Odebranie odpowiedzi
        # TODO: odebrać odpowiedź z serwera i zapisać ją w zmiennej received

        #received = ''
        received = s.recv(2048)
        print "[Wiadomośc serwerowa] " + received

        #logger.info(u'odebrano "{0}"'.format(received))
        #received2 = ''

        #logger.info(u'wysyłam "{0}"'.format(message2))
        s.sendall(message2)
        received2 = s.recv(2048)
        print "[Wiadomośc serwerowa] " + received2
        #logger.info(u'odebrano "{0}"'.format(received2))


        received3 = s.recv(2048)
        print "[Wiadomośc serwerowa] Wynik dodawania: " + received3

        #logger.info(u'odebrano "{0}"'.format(received3))
    finally:
        # Zamknięcie połączenia na zakończenie działania
        # TODO: zamknąć połączenie sieciowe

        s.close()
        #logger.info(u'połączenie zostało zakończone')
        #return received3
        return received3

def tryParseInt(str):
    isValid = False
    try:
        returnValue = int(str)
        isValid = True
    except ValueError:
        returnValue = 0
        print "Wprowadz wlasciwa liczbe"
    return returnValue, isValid

if __name__ == '__main__':
    message = ''
    message2 = ''

    keepProgramRunning = True

    while keepProgramRunning:
        print "\nWitaj w serwerowym kalkulatorze. Wybierz co chcesz zrobic:"

        print "1: Dodawanie"
        print "2: Wyjdz z aplikacji"

        choice = raw_input()

        if choice == "1":

            isFirstNumberValid = False
            while not isFirstNumberValid:
                message = raw_input("Wprowadz pierwsza liczbe: ")
                firstNumber, isFirstNumberValid = tryParseInt(message)



            isSecondNumberValid = False
            while not isSecondNumberValid:
                message2 = raw_input("Wprowadz druga liczbe: ")
                secondNumber, isSecondNumberValid = tryParseInt(message2)




            # logging.config.fileConfig('logging.conf')
            # logger = logging.getLogger('sum_client')

            print "\nWynik dodawania:"
            client(message, message2)
            #print firstNumber + secondNumber

        elif choice == "2":
            print "Koniec aplikacji"
            keepProgramRunning = False
