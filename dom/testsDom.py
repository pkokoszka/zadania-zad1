# -*- coding: utf-8 -*-

import socket
import unittest

from sum_client import client


class SumTestCase(unittest.TestCase):
    """testy dla klienta i serwera echo"""
    sending_msg = u'wysyłam "{0}"'
    received_msg = u'odebrano "{0}"'

    def setUp(self):
        """ustawienia dla testów"""
        #self.logger = LoggerStub()

    def test_messages_should_be_int(self):
        message = "dasdas"
        message2 = "dasdas"

        try:
            client(message, message2)

        except ValueError:
            self.fail("Program nie powinien przyjmować stringów")

    def test_sum_int(self):
        liczba1Str = "35"
        liczba2Str = "55"

        liczba1 = 35
        liczba2 = 55
        oczekiwany_wynik = liczba1 + liczba2
        try:
            wynik = client(liczba1Str, liczba2Str)
            self.assertEqual(int(wynik), oczekiwany_wynik)
        except socket.error as e:
            if e.errno == 61:
                msg = u'Blad: {0}, czy serwer dziala?'
                self.fail(msg.format(e.strerror))
            else:
                self.fail(u'Nieznany blad: {0}'.format(str(e)))




if __name__ == '__main__':
    unittest.main()